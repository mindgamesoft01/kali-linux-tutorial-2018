package com.mindgame.kali.linux.tutorial1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;



public class MainActivity extends Activity {
    RecyclerView recyclerView;

    String val;
    TextView heading;
    LinearLayout layout, strip, layout1, strip1;
    AdClass ad = new AdClass();
    Button btn1,btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9,btn10,btn11,btn12,btn13;


    Boolean connected;


    TextView terms_privacy;

    ScrollView scrollView;
    public static String []array_items;
    NetworkStatusCheck NC= NetworkStatusCheck.getInstance();
    singleton_images sc=singleton_images.getInstance();
    String app_status = sc.app_mode();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        Bundle b=getIntent().getExtras();
//        val=b.getString("flag");

        layout = (LinearLayout) findViewById(R.id.admob);
        strip = ad.layout_strip(this);
        layout.addView(strip);
        ad.AdMobBanner1(this);



        initViews();
        if (app_status.equals("TEST")) {

            recyclerView.setVisibility(View.INVISIBLE);

        }
        if (app_status.equals("PROD"))

        {
            recyclerView.setHasFixedSize(true);
            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 1);
            recyclerView.setLayoutManager(layoutManager);
            DataAdapter adapter = new DataAdapter(getApplicationContext(), array_items);
            recyclerView.setAdapter(adapter);

        }



    }



    private void initViews() {
        recyclerView=(RecyclerView)findViewById(R.id.recyclerview);
        array_items=getResources().getStringArray(R.array.array_items);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i=new Intent(MainActivity.this,Launcher_Activity.class);
        startActivity(i);
    }
}
